import React, { Suspense, lazy } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { CircularProgress } from "@material-ui/core";
import SettingsContext from "./context";
import "./App.scss";

const NotFound = lazy(() => import("./pages/NotFound"));
const Auth = lazy(() => import("./pages/Authorization"));
const Register = lazy(() => import("./pages/Registration"));
const Profile = lazy(() => import("./pages/Profile"));
const Company = lazy(() => import("./pages/Company"));
const CreateProject = lazy(() => import("./pages/CreateProject"));
const Statistics = lazy(() => import("./pages/Statistics"));
const Workers = lazy(() => import("./pages/Workers"));
const Clients = lazy(() => import("./pages/Clients"));
const Projects = lazy(() => import("./pages/Projects"));
const NewProjects = lazy(() => import("./pages/NewProjects"));

const PrivateRoute = ({ component: Component, errors, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        !localStorage.getItem("user") ? (
          <Redirect to="/auth" />
        ) : (
            <Component {...props} />
          )
      }
    />
  );
};

const Main = () => (
  <Suspense fallback={<CircularProgress className="center-item" />}>
    <Switch>
      <Redirect exact from="/" to="/projects" />
      <Route exact path="/auth" component={Auth} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/company" component={Company} />
      <PrivateRoute exact path="/profile" component={Profile} />
      <PrivateRoute exact path="/create" component={CreateProject} />
      <PrivateRoute exact path="/statistics" component={Statistics} />
      <PrivateRoute exact path="/workers" component={Workers} />
      <PrivateRoute exact path="/clients" component={Clients} />
      <PrivateRoute exact path="/projects" component={Projects} />
      <PrivateRoute exact path="/projects/new" component={NewProjects} />
      <Route component={NotFound} />
    </Switch>
  </Suspense>
)

export default class App extends React.Component {
  static contextType = SettingsContext;

  async UNSAFE_componentWillMount() {
    await this.context.getCurrentUser();
  }

  render() {
    return (
      <div className="App">
        <Main />
      </div>
    );
  }
}
