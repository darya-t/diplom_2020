import React from "react";
import {
	IconButton,
	InputAdornment,
	OutlinedInput,
	InputLabel,
	FormControl,
	FormHelperText,
} from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import "./styles.scss";

export default class PasswordField extends React.Component {
	state = {
		user: {
			email: "",
			password: "",
			passwordRepeat: "",
			role: 0,
			fio: "",
			description: "",
			phone: "",
		},
		isPwdShow1: false,
		isPwdShow2: false,
		message: "",
		errors: {
			email: false,
			password: false,
			passwordRepeat: false,
			fio: false,
			phone: false,
		}
	}

	handleClickShowPassword = (field) => {
		this.setState({ [field]: !this.state[field] });
	}

	render() {
		const {
			label,
			error,
			helperText,
			isPwdShow,
			valueField,
			isPwdShowField,
			className,
			labelWidth,
			onKeyPress,
			onChange,
			handleClickShowPassword,
		} = this.props;

		return (
			<FormControl variant="outlined" className={className}>
				<InputLabel	required htmlFor={`outlined-adornment-password-${valueField}`} error={error}>
					{label}
				</InputLabel>
				<OutlinedInput
					id={`outlined-adornment-password-${valueField}`}
					type={isPwdShow ? "text" : "password"}
					error={error}
					onKeyPress={onKeyPress}
					onChange={(e) => onChange(e, valueField)}
					labelWidth={labelWidth}
					endAdornment={
						<InputAdornment position="end">
							<IconButton
								aria-label="toggle password visibility"
								onClick={() => handleClickShowPassword(isPwdShowField)}
							>
								{isPwdShow ? <Visibility /> : <VisibilityOff />}
							</IconButton>
						</InputAdornment>
					}
				/>
				<FormHelperText
					htmlFor={`outlined-adornment-password-${valueField}`}
					error={error && helperText.length < 40}
				>
					{helperText}
				</FormHelperText>
			</FormControl>
		);
	}
}
