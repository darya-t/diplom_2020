import React from "react";
import {
  AppBar,
  Toolbar,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import "./styles.scss";

export default class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <AppBar position="static">
          <Toolbar>
            <div className="footer-div row">
              <div>
                <p className="without_margins">Just IT ©</p>
                <Link
                  className="without_margins"
                  target="_blank"
                  to="/company"
                >
                  Ссылка на сайт компании
                  </Link>
              </div>
              <div>
                <p className="without_margins">Сделано Дарьей Туркиной</p>
                <a
                  className="without_margins"
                  href="https://vk.com/dastockgolskaya"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Дарья в ВК
                  </a>
              </div>
              <div>
                <p className="without_margins">МОП ЭВМ ИКТИБ ИТА ЮФУ</p>
                <a
                  className="without_margins"
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.sfedu.ru/"
                >
                  Ссылка на сайт университета
                  </a>
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
