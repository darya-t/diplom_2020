import React from "react";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  Badge,
} from "@material-ui/core";
import {
  MonetizationOn,
  AssignmentInd,
  InsertChart,
  Dns,
  LibraryAdd,
  People,
} from "@material-ui/icons";
import SettingsContext from "../context";
import "./styles.scss";

const menuPoints = [
  {
    title: "Проекты",
    link: "/projects",
    userTypes: [1, 2, 3, 4],// все
    icon: "orders",
  },
  {
    title: "Статистика",
    link: "/statistics",
    userTypes: [3],// сотр
    icon: "statistics",
  },
  {
    title: "Сотрудники",
    link: "/workers",
    userTypes: [3, 4],// сотр и адм
    icon: "workers",
  },
  {
    title: "Клиенты",
    link: "/clients",
    userTypes: [3, 4],// сотр и адм
    icon: "clients",
  },
  {
    title: "Новые заказы",
    link: "/projects/new",
    userTypes: [2, 3], // сотр и РМ
    icon: "newOrders",
  },
  {
    title: "Создать заказ",
    link: "/create",
    userTypes: [1],// клиент
    icon: "add",
  },
];

export default class Menu extends React.Component {
  static contextType = SettingsContext;

  getIcon = (icon) => {
    switch (icon) {
      case "orders":
        return <Dns />;
      case "statistics":
        return <InsertChart />;
      case "workers":
        return <AssignmentInd />;
      case "newOrders":
        return <MonetizationOn />;
      case "add":
        return <LibraryAdd />;
      case "clients":
        return <People />;
      default: return;
    }
  }

  render() {
    const { user, newProjects } = this.context;

    return (
      <div
        role="presentation"
        className="menu"
      >
        <div className="menu-div">
          <Divider />
          <h1 style={{ margin: "13px" }}>Меню</h1>
          <Divider />
          <List>
            {menuPoints.map(({ title, icon, userTypes, link }) => {
              if (userTypes.includes(user.role)) return (
                <div key={title}>
                  <ListItem button key={title} onClick={() => this.props.onClick(link)}>
                    <ListItemIcon>
                      {icon === "newOrders"
                        ? <Badge color="secondary" badgeContent={newProjects.length}>
                          {this.getIcon(icon)}
                        </Badge>
                        : this.getIcon(icon)
                      }
                    </ListItemIcon>
                    <ListItemText primary={title} />
                  </ListItem>
                  <Divider />
                </div>
              )
            })}
          </List>
        </div>
      </div>
    )
  }
}
