import React from "react";
import { Snackbar, IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

export default class Snack extends React.Component {
	getColor(type) {
		switch (type) {
			case "error":
				return "red";
			case "warning":
				return "yellow";
			case "success":
				return "green";
			case "info":
				return "blue";
			default:
				return "lightgrey";
		}
	}

	render() {
		const { open, handleClose, text, vertical, horizontal, type } = this.props;
		return (
			<Snackbar
				open={open}
				autoHideDuration={6000}
				onClose={handleClose}
				className={`snack ${type}`}
				anchorOrigin={{
					vertical: vertical || "top",
					horizontal: horizontal || "center"
				}}
				message={text}
				action={
					<React.Fragment>
						<IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
							<CloseIcon fontSize="small" />
						</IconButton>
					</React.Fragment>
				}
			/>
		);
	}
}
