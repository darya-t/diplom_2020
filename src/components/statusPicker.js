import React from "react";
import {
	Select,
	MenuItem,
} from "@material-ui/core";

export const statuses = [
	"Новый",
	"На рассмотрении",
	"Выполняется",
	"На удержании",
	"Отменён",
	"Выполнен",
	"Закрыт",
];

export default class statusPicker extends React.Component {
	state = {
		defaultStatus: this.props.defaultStatus,
	}

	handleStatusChange = (e) => {
		this.props.onChange(e.target.value, "status");
		this.setState({ defaultStatus: e.target.value });
	}

	render() {
		const { defaultStatus } = this.state;
		const { readOnly, withNull } = this.props;

		return (
			<Select
				labelId="demo-simple-select-outlined-label"
				id="demo-simple-select-outlined"
				variant="outlined"
				value={defaultStatus}
				fullWidth
				inputProps={{ readOnly: readOnly ? readOnly : false }}
				onChange={(e) => this.handleStatusChange(e)}
			>
				{
					withNull && <MenuItem value="Не выбрано" >Не выбрано</MenuItem>
				}
				{statuses.map(name => (
					<MenuItem key={name} value={name}>{name}</MenuItem>
				))}
			</Select>
		)
	}
}