import React from "react";
import {
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TablePagination,
	TableRow,
	Divider,
	Button,
	Tooltip,
	CircularProgress,
} from "@material-ui/core";
import Modal from "./projectInfoModal";
import UserInfoModal from "./userInfoModal";
import SettingsContext from "../context";

export default class StickyHeadTable extends React.Component {
	static contextType = SettingsContext;

	state = {
		page: 0,
		rowsPerPage: 5,
		open: false,
		project: {},
		user: {},
		loading: {
			status: false,
			project: "",
		},
		openUserInfoModal: false,
	}

	handleChangePage = (event, newPage) => {
		this.setState({ page: newPage });
	}

	handleChangeRowsPerPage = (event) => {
		this.setState({ page: 0, rowsPerPage: +event.target.value });
	}

	handleOpenPopup = (user) => {
		this.setState({ openUserInfoModal: true, user });
	}

	cutLongName = (name) => {
		if (name.length > 40) {
			return `${name.slice(0, 40)}...`;
		} else {
			return name;
		}
	}

	handleProjectPopupOpen = (project) => {
		const manager = this.context.workers.find(worker => worker._id === project.manager);
		const client = this.context.clients.find(client => client._id === project.client);
		this.setState({ open: true, project: { ...project, client, manager } });
	}

	getProject = async (id) => {
    this.setState({
			loading: {
				status: true,
				project: id,
			},
		});
		const result = await this.context.getProjectByPM(id);
		this.props.updateTable({ ...result });
		this.setState({
			loading: {
				status: false,
				project: "",
			},
		});
	}

	render() {
		const { headers, type, data } = this.props;
		const {
			page,
			rowsPerPage,
			open,
			project,
			openUserInfoModal,
			user,
      loading,
		} = this.state;

		return (
			<Paper>
				<TableContainer>
					<Table stickyHeader aria-label="sticky table">
						<TableHead>
							<TableRow>
								{headers.map((header, index) => (
									<TableCell
										key={`head-${index}`}
										align="left"
									>
										{header}
									</TableCell>
								))}
							</TableRow>
						</TableHead>
						<TableBody>
							{
								type === "workers" || type === "clients" ?
									data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((user, idx) => (
										<TableRow
											hover
											role="checkbox"
											tabIndex={-1}
											key={`row-${idx}`}
											onClick={() => this.handleOpenPopup(user)}
										>
											<TableCell align="center">
												{idx + 1}
											</TableCell>
											<TableCell align="left">
												{user.fio}
											</TableCell>
											<TableCell align="left">
												{user.email}
											</TableCell>
											<TableCell align="left">
												{user.phone}
											</TableCell>
										</TableRow>
									))
									: data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((project, idx) => (
										<TableRow
											hover
											role="checkbox"
											tabIndex={-1}
											key={`row-${idx}`}
										>
											<TableCell align="left" onClick={() => this.handleProjectPopupOpen(project)}>
												{idx + 1}
											</TableCell>
											<TableCell align="left" onClick={() => this.handleProjectPopupOpen(project)}>
												{this.cutLongName(project.name)}
											</TableCell>
											<TableCell align="left" onClick={() => this.handleProjectPopupOpen(project)}>
												{project.status}
											</TableCell>
											{
												type === "new" &&
												<TableCell align="center">
													<div className="auth-form__btn-wrapper">
														<Tooltip
															arrow
															title="Нажимая кнопку, Вы будете назначены исполнителем этого проекта"
														>
															<Button
																variant="outlined"
																color="primary"
																style={{
																	fontSize: "smaller",
																}}
																onClick={() => this.getProject(project._id)}
																disabled={this.context.user.role !== 2
																	|| (loading.status && loading.project === project._id)
																}
															>
																Взяться за проект
															</Button>
														</Tooltip>
													{loading.status && loading.project === project._id
														&& <CircularProgress size={24} className="btn-progress" />}
												</div>
												</TableCell>
											}
										</TableRow>
									))
							}
						</TableBody>
					</Table>
				</TableContainer>
				<Divider />
				<TablePagination
					rowsPerPageOptions={[3, 5, 7]}
					component="div"
					count={data.length}
					rowsPerPage={rowsPerPage}
					page={page}
					onChangePage={this.handleChangePage}
					onChangeRowsPerPage={this.handleChangeRowsPerPage}
				/>
				<Modal
					open={open}
					project={project}
					updateProjects={this.props.updateTable}
					onClose={() => this.setState({ open: false, project: {} })}
				/>
				<UserInfoModal
					user={user}
					open={openUserInfoModal}
					onClose={() => this.setState({ openUserInfoModal: false, user: {} })}
				/>
			</Paper>
		)
	}
}