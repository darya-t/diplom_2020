import React from "react";
import {
	Button,
	Popover,
	Divider,
	TextField
} from "@material-ui/core";
import { FilterList } from "@material-ui/icons";
import StatusPicker from "./statusPicker";
import SettingsContext from "../context";

const fioInputs = [
	{
		label: "Клиент",
		field: "client",
		noType: 1,
	},
	{
		label: "Менеджер",
		field: "manager",
		noType: 2,
	},
];

const initialFilter = {
	created_from: "",
	created_to: "",
	name: "",
	status: "Не выбрано",
	client: "",
	manager: "",
};

const cleanFilter = {
	created_from: "",
	created_to: "",
	name: "",
	status: "",
	client: "",
	manager: "",
};

export default class filter extends React.Component {
	static contextType = SettingsContext;

	state = {
		anchorEl: null,
		open: false,
		filter: initialFilter,
	}

	UNSAFE_componentWillMount() {
		const date = this.getCurrentDate();
		this.setState({ filter: { ...this.state.filter, created_from: date, created_to: date } });
	}

	handleStatusChange = (e) => {
		this.props.onChange(e.target.value, "status");
		this.setState({ defaultStatus: e.target.value });
	}

	toggleMenu = (e) => {
		this.setState({ open: true, anchorEl: e.currentTarget });
	}

	handleMenuClose = () => {
		const date = this.getCurrentDate();
		this.setState({
			open: false,
			anchorEl: null,
			filter: {
				...initialFilter,
				created_from: date,
				created_to: date
			},
		});
	}

	getCurrentDate = () => {
		let now = new Date();
		const day = now.getDate() > 9 ? now.getDate() : `0${now.getDate()}`;
		const month = now.getMonth() > 9 ? now.getMonth() : `0${now.getMonth() + 1}`;
		now = `${now.getFullYear()}-${month}-${day}`;
		return now;
	}

	setNewValue = (value, field) => {
		this.setState({ filter: { ...this.state.filter, [field]: value } });
	}

	clearFilter = () => {
		this.props.onFilter(cleanFilter);
		this.setState({ filter: initialFilter });
		this.handleMenuClose();
	}

	setFilter = () => {
		let filter = this.state.filter;
		if (filter.status === "Не выбрано") {
			filter.status = "";
		}
		this.props.onFilter(filter);
	}

	render() {
		const { open, anchorEl, filter } = this.state;
		const { user } = this.context;

		return (
			<>
				<Button className="profile-btn" onClick={(e) => this.toggleMenu(e)}>
					<FilterList />
				</Button>
				<Popover
					open={open}
					anchorEl={anchorEl}
					onClose={this.handleMenuClose}
					className="filter"
				>
					<p>Выберите параметры фильтрации</p>
					<Divider />
					<TextField
						onChange={(e) => this.setNewValue(e.target.value, "name")}
						variant="outlined"
						className="input"
						label="Название"
						fullWidth
					/>
					<p>Статус</p>
					<StatusPicker
						defaultStatus={filter.status}
						onChange={this.setNewValue}
						withNull={true}
					/>
					<p>Дата начала</p>
					<TextField
						id="date"
						type="date"
						className="input"
						label={filter.created_from ? "MM/DD/YYYY" : ""}
						variant="outlined"
						defaultValue={filter.created_from}
						onChange={(e) => this.setNewValue(e.target.value, "created_from")}
						fullWidth
					/>
					<p>Дата окончания</p>
					<TextField
						id="date"
						type="date"
						className="input"
						label={filter.created_to ? "MM/DD/YYYY" : ""}
						variant="outlined"
						defaultValue={filter.created_to}
						onChange={(e) => this.setNewValue(e.target.value, "created_to")}
						fullWidth
					/>
					{fioInputs.map(el => {
						if (user.role !== el.noType) {
							return (
								<TextField
									onChange={(e) => this.setNewValue(e.target.value, el.field)}
									key={`fio-${el.label}`}
									className="input"
									variant="outlined"
									label={el.label}
									fullWidth
								/>
							)
						}
					})}
					<div className="row">
						<Button
							variant="outlined"
							color="primary"
							className="auth-button"
							onClick={this.clearFilter}
						>
							Очистить фильтр
						</Button>
					</div>
					<div className="row">
						<Button
							variant="outlined"
							color="primary"
							className="auth-button"
							onClick={this.setFilter}
						>
							Найти
						</Button>
					</div>
				</Popover>
			</>
		)
	}
}