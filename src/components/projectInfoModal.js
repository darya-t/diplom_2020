import React from "react";
import {
	Divider,
	Modal,
	TextField,
	Button,
  CircularProgress,
} from "@material-ui/core";
import UserInfoModal from "./userInfoModal";
import StatusPicker from "./statusPicker";
import SettingsContext from "../context";
import { checkDeadlineDateValid } from "../pages/CreateProject";
import { getUsualDate } from "../pages/Profile";
import "./styles.scss";

export default class ProjectInfoModal extends React.Component {
	static contextType = SettingsContext;

	state = {
		openUserInfoModal: false,
		isClient: this.context.user.role === 1,
		isManager: this.context.user.role === 2,
		projectChanges: { ...this.props.project },
		errors: {
			deadline: false,
		},
		loading: false,
		user: {},
	}

	setNewValue = (value, field) => {
		this.setState({ projectChanges: { ...this.state.projectChanges, [field]: value } });
		if (field === "deadline") {
			this.setState({ errors: { deadline: false } });
		}
	}

	handleOpenPopup = (user) => {
		this.setState({ openUserInfoModal: true, user });
	}

	saveChanges = async () => {
		this.setState({ loading: true });
		const { projectChanges } = this.state;
		const { project } = this.props;
		const isDateError = projectChanges.deadline
			&& !checkDeadlineDateValid(project.created_at, projectChanges.deadline);
		if (isDateError) {
			this.setState({
				errors: {
					deadline: isDateError,
				},
				loading: false,
			});
			return;
		}
		let changedProject = {
			...project,
			...projectChanges,
			client: project.client._id,
		}
		if (!project.manager) {
			delete changedProject.manager;
		} else {
			changedProject.manager = project.manager._id;
		}
		const { message, success } = await this.context.updateProjectInfo(changedProject);
		this.props.updateProjects({ message, success });
		this.setState({ loading: false });
	}

	getChangingAbility = (project) => {
		const { isClient, isManager } = this.state;
		const { user } = this.context;

		return !(isClient
			|| (isManager
				&& Object.keys(project).length
				&& project.manager
				&& project.manager._id === user._id
			)
		);
	}

	render() {
		const { open, project, onClose } = this.props;
		const { client, manager } = project;
		const {
			openUserInfoModal,
			isClient,
			isManager,
			errors,
			user,
			loading,
		} = this.state;

		return (
			<>
				<Modal
					open={open}
					onClose={() => onClose()}
					aria-labelledby="simple-modal-title"
					aria-describedby="simple-modal-description"
				>
					<div className="project-info-modal">
						<h2>{project.name}</h2>
						<Divider style={{ margin: "0 0 10px 0" }} />
						<table cellPadding="10px" className="table_profile">
							<tbody>
								{(client && !isClient) &&
									<tr>
										<td className="small-col">
											Клиент
									</td>
										<td>
											<TextField
												defaultValue={client.fio}
												variant="outlined"
												onClick={() => this.handleOpenPopup(client)}
												InputProps={{
													readOnly: true,
												}}
												className="pointer"
												fullWidth
											/>
										</td>
									</tr>
								}
								{!isManager &&
									<tr>
										<td className="small-col">
											Менеджер проекта
										</td>
										<td>
											{manager ?
												<TextField
													defaultValue={manager.fio}
													variant="outlined"
													onClick={() => this.handleOpenPopup(manager)}
													InputProps={{
														readOnly: true,
													}}
													className="pointer"
													fullWidth
												/>
												:
												<TextField
													defaultValue="Ещё не назначен"
													variant="outlined"
													InputProps={{
														readOnly: true,
													}}
													fullWidth
												/>}
										</td>
									</tr>
								}
								<tr>
									<td>Начало</td>
									<td>Конец</td>
								</tr>
								<tr>
									<td>
										<TextField
											type="date"
											label="MM/DD/YYYY"
											variant="outlined"
											defaultValue={`${project.created_at}`.split("T")[0]}
											InputProps={{
												readOnly: true,
											}}
											fullWidth
										/>
									</td>
									<td>
										<TextField
											id="date"
											type="date"
											label={project.deadline ? "MM/DD/YYYY" : ""}
											variant="outlined"
											error={errors.deadline}
											onChange={(e) => this.setNewValue(e.target.value, "deadline")}
											helperText={errors.deadline && "Дата не может быть раньше начала проекта"}
											defaultValue={`${project.deadline}`.split("T")[0]}
											InputProps={{
												readOnly: this.getChangingAbility(project),
											}}
											fullWidth
										/>
									</td>
								</tr>
								<tr>
									<td className="small-col">Статус</td>
									<td>
										<StatusPicker
											defaultStatus={project.status}
											onChange={this.setNewValue}
											readOnly={this.context.user.role !== 2 || this.getChangingAbility(project)}
											withNull={false}
										/>
									</td>
								</tr>
								<tr>
									<td className="small-col">Описание</td>
									<td>
										<TextField
											defaultValue={project.description}
											onChange={(e) => this.setNewValue(e.target.value, "description")}
											multiline
											rowsMax={7}
											InputProps={{
												readOnly: this.getChangingAbility(project),
											}}
											variant="outlined"
											fullWidth
										/>
									</td>
								</tr>
							</tbody>
						</table>
						<p style={{ textAlign: "center" }}>
							{project.updated_at && `Дата последних изменений: ${getUsualDate(project.updated_at)}`}
						</p>
						<div className="row">
							{(isClient || isManager) &&
								<div className="auth-form__btn-wrapper auth-button">
									<Button
										variant="outlined"
										color="primary"
										onClick={this.saveChanges}
										disabled={this.getChangingAbility(project) || loading}
									>
										Сохранить
									</Button>
									{loading && <CircularProgress size={24} className="btn-progress" />}
								</div>
							}
							<div className="auth-button">
								<Button
									variant="outlined"
									color="primary"
									onClick={onClose}
								>
									Ok
								</Button>
							</div>
						</div>
					</div >
				</Modal >
				<UserInfoModal
					user={user}
					open={openUserInfoModal}
					onClose={() => this.setState({ openUserInfoModal: false, user: {} })}
				/>
			</>
		)
	}
}