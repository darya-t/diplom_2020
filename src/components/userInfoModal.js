import React from "react";
import {
	Divider,
	Modal,
	TextField,
	Button,
} from "@material-ui/core";
import SettingsContext from "../context";
import { getUsualDate } from "../pages/Profile";
import "./styles.scss";

export default class UserInfoModal extends React.Component {
	static contextType = SettingsContext;

	state = {
		user: {},
	}

	render() {
		const { open, user, onClose } = this.props;

		return (
			<Modal
				open={open}
				onClose={() => onClose()}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
			>
				<div className="project-info-modal">
					<h2>{user.fio}</h2>
					<Divider style={{ margin: "0 0 10px 0" }} />
					<table cellPadding="10px" className="table_profile">
						<tbody>
							<tr>
								<td className="small-col">Описание</td>
								<td>
									<TextField
										defaultValue={user.description}
										variant="outlined"
										multiline
										rowsMax={10}
										InputProps={{
											readOnly: true,
										}}
										fullWidth
									/>
								</td>
							</tr>
							<tr>
								<td className="small-col">Email:</td>
								<td>
									<TextField
										defaultValue={user.email}
										variant="outlined"
										InputProps={{
											readOnly: true,
										}}
										fullWidth
									/>
								</td>
							</tr>
							<tr>
								<td className="small-col">Тел.:</td>
								<td>
									<TextField
										defaultValue={user.phone}
										variant="outlined"
										InputProps={{
											readOnly: true,
										}}
										fullWidth
									/>
								</td>
							</tr>
							<tr>
								<td className="small-col">Проектов:</td>
								<td>
									<TextField
										defaultValue={user.projects}
										variant="outlined"
										InputProps={{
											readOnly: true,
										}}
										fullWidth
									/>
								</td>
							</tr>
						</tbody>
					</table>
					<p style={{ textAlign: "center" }}>
						{user.created_at && `Дата регистрации: ${getUsualDate(user.created_at)}`}
					</p>
					<Button
						variant="outlined"
						color="primary"
						className="auth-button"
						onClick={onClose}
					>
						Ок
          </Button>
				</div >
			</Modal >
		)
	}
}