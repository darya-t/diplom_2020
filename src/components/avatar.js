import React from "react";
import { Avatar } from "@material-ui/core";
import SettingsContext from "../context";

export default class UserAvatar extends React.Component {
	static contextType = SettingsContext;

	render() {
		const { width } = this.props;

		return (
			<>
				<Avatar style={{
					width: width ? width : "30px",
					height: width ? width : "30px",
					color: "white",
					backgroundColor: "#3f51b5",
					border: "1px solid white",
					margin: "5px",
				}}>
					<p style={{
						textTransform: "uppercase",
						fontSize: width ? "xx-large" : "0.7rem",
						fontWeight: "bold"
					}}>
						{this.context.user.letters}
					</p>
				</Avatar>
			</>
		);
	}
}
