import React from "react";
import {
  AppBar,
  Button,
  Toolbar,
  Divider,
  Menu,
  ListItemIcon,
  MenuItem,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import {
  AccountCircleOutlined as AccountCircleOutlinedIcon,
  ExitToAppOutlined as ExitToAppOutlinedIcon,
  AdbOutlined,
} from "@material-ui/icons";
import Avatar from "./avatar";
import SettingsContext from "../context";
import "./styles.scss";

const userMenu = [
  {
    name: "Профиль",
    icon: "profile",
    path: "/profile",
  },
  {
    name: "Выйти",
    icon: "exit",
    path: "/auth",
  }
];

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
    width: "150px",
  },
})(props => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles(theme => ({
  root: {
    "&:focus": {
      backgroundColor: theme.palette.primary.main,
      "& .MuiListItemIcon-root, & .MuiListItemText-primary": {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

export default class Header extends React.Component {
  static contextType = SettingsContext;
  state = {
    anchorEl: null,
  }

  getIcon = (iconName) => {
    switch (iconName) {
      case "profile":
        return <AccountCircleOutlinedIcon fontSize="small" />;
      case "exit":
        return <ExitToAppOutlinedIcon fontSize="small" />;
      default:
        return <AdbOutlined fontSize="small" />;
    }
  }

  toggleMenu = (e) => {
    this.setState({ anchorEl: e.currentTarget });
  }

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
  }

  getUserName = () => {
    const { email } = this.context.user;
    return email ? email.split("@")[0] : "";
  }

  render() {
    const { anchorEl } = this.state;

    return (
      <div className="header">
        <AppBar position="static">
          <Toolbar className="header-div">
            <div className="company-name">
              <img
                src="https://x-lines.ru/letters/i/cyrillicscript/2607/ffffff/60/1/jj4zg7byjfky.png"
                alt="logo-fail"
                className="company-name_logo"
              />
            </div>
            <Divider orientation="vertical" flexItem />
            <Button className="profile-btn" onClick={this.toggleMenu}>
              <Avatar />
              <p className="not-first">{this.getUserName()}</p>
            </Button>
            <StyledMenu
              id="customized-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={this.handleMenuClose}
            >
              {
                userMenu.map((point, idx) => {
                  return (
                    <div key={point.name}>
                      {idx !== 0 && <Divider />}
                      <StyledMenuItem onClick={() => this.props.onClick(point.path)}>
                        <ListItemIcon>
                          {this.getIcon(point.icon)}
                        </ListItemIcon>
                        {point.name}
                      </StyledMenuItem>
                    </div>
                  )
                })
              }
            </StyledMenu>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
