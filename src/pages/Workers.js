import React from "react";
import Menu from "../components/menu";
import Header from "../components/header";
import Footer from "../components/footer";
import Table from "../components/table";
import SettingsContext from "../context";
import "./styles.scss";

export default class Workers extends React.Component {
  static contextType = SettingsContext;

  state = {
    workers: this.context.workers,
  }

  async UNSAFE_componentWillMount() {
    const { user } = this.context;
    if (user.role !== 3 && user.role !== 4) {
      this.props.history.push("/notfound");
    }
  }

  handleMenuPointClick = (path) => {
    this.props.history.push(path);
  }

  handleDropdownMenuClick = (path) => {
    this.props.history.push(path);
    if (path === "/auth") {
      this.context.logout();
    }
  }

  render() {
    const { workers } = this.state;

    return (
      <div>
        <div>
          <Header onClick={this.handleDropdownMenuClick} />
          <div className="app-content row">
            <Menu onClick={this.handleMenuPointClick} />
            <div className="tables-module">
              <h1>Менеджеры проектов</h1>
              <p>Нажмите на строку для просмотра подробной информации</p>
              <Table
                headers={["No", "ФИО", "Email", "Тел."]}
                data={workers}
                type="workers"
              />
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
