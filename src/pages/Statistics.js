import React from "react";
import { Button } from "@material-ui/core";
import Menu from "../components/menu";
import Header from "../components/header";
import Footer from "../components/footer";
import SettingsContext from "../context";
import { Pie, Bar } from "react-chartjs-2";
import { statuses } from "../components/statusPicker";
import * as ReactExport from "react-data-export";
import Filter from "../components/filter";
import "./styles.scss";

const ExcelFile = ReactExport.default.ExcelFile;
const ExcelSheet = ReactExport.modules.ExcelSheet;

export default class Statistics extends React.Component {
  static contextType = SettingsContext;
  state = {
    projects: this.context.allProjects,
    maxYear: new Date().getFullYear(),
  };
  currentYear = new Date().getFullYear();
  currentMonth = new Date().getMonth();
  labels = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

  async componentDidMount() {
    const role = await this.context.user.role;
    if (role !== 3) {
      this.props.history.push("/notfound");
    }
  }

  handleMenuPointClick = (path) => {
    this.props.history.push(path);
  }

  handleDropdownMenuClick = (path) => {
    this.props.history.push(path);
    if (path === "/auth") {
      this.context.logout();
    }
  }

  getThisMonthProjects = (projects, type) => {
    return projects.filter(project => {
      const month = new Date(project[type]).getMonth();
      return month === this.currentMonth;
    }).length;
  }

  getPieData = () => {
    const { projects } = this.state;
    let list = {
      new: 0,
      pending: 0,
      in_progress: 0,
      on_hold: 0,
      calnceled: 0,
      done: 0,
      closed: 0,
    };
    projects.forEach(el => {
      switch (el.status) {
        case statuses[0]:
          list.new++;
          break;
        case statuses[1]:
          list.pending++;
          break;
        case statuses[2]:
          list.in_progress++;
          break;
        case statuses[3]:
          list.on_hold++;
          break;
        case statuses[4]:
          list.calnceled++;
          break;
        case statuses[5]:
          list.done++;
          break;
        case statuses[6]:
          list.closed++;
          break;
        default:
          break;
      }
    });
    const dataArr = Object.keys(list).map(elem => list[elem]);
    return {
      labels: statuses,
      datasets: [{
        data: dataArr,
        borderColor: "#3f51b5",
        borderWidth: 1,
        backgroundColor: [
          "#1DC796",
          "#2196F3",
          "#9782C7",
          "#FDDE8E",
          "#FF9800",
          "#B96193",
          "#AFB0B2",
        ]
      }],
    }
  }

  getBarDataStart = () => {
    const { projects, maxYear } = this.state;
    let projectsCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    projects.forEach(pr => {
      const month = new Date(pr.created_at).getMonth();
      const year = new Date(pr.created_at).getFullYear();
      if (year === maxYear) {
        projectsCount[month]++;
      }
    });
    return {
      labels: this.labels,
      datasets: [{
        data: projectsCount,
        backgroundColor: "#3f51b5",
      }],
    }
  }

  getBarDataFinish = () => {
    const { projects, maxYear } = this.state;
    let projectsCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    projects.forEach(pr => {
      const month = new Date(pr.deadline).getMonth();
      const year = new Date(pr.deadline).getFullYear();
      if (year === maxYear) {
        projectsCount[month]++;
      }
    });
    return {
      labels: this.labels,
      datasets: [{
        data: projectsCount,
        backgroundColor: "#3f51b5",
      }],
    }
  }

  getExcelData = () => {
    const barDataStart = this.getBarDataStart();
    const barDataFinish = this.getBarDataFinish();
    const pieData = this.getPieData();
    let columns = ["Направление"].concat(barDataStart.labels.concat(statuses));
    let data = [];
    data[0] = ["Начинается"].concat(barDataStart.datasets[0].data.concat(pieData.datasets[0].data));
    data[1] = ["Заканчивается"].concat(barDataFinish.datasets[0].data.concat(pieData.datasets[0].data));

    return [{
      columns,
      data,
    }];
  }

  setFilteredData = (filter) => {
    const {
      name,
      client,
      manager,
      created_from,
      created_to,
      status,
    } = filter;
    let maxYear = 0;
    const isFilter = name || client || manager || created_from || created_to || status;
    const projects = this.context.allProjects;
    let result = projects;
    if (isFilter) {
      if (name) {
        result = result.filter(project => project.name.toLowerCase().includes(name.toLowerCase()));
      }
      if (client) {
        result = result.filter(project => this.getUserName(project.client, "client").includes(client));
      }
      if (manager) {
        result = result.filter(project => project.manager && this.getUserName(project.manager, "manager").includes(manager));
      }
      if (created_from) {
        result = result.filter(project => this.getDate(project.created_at) >= this.getDate(created_from));
      }
      if (created_to) {
        result = result.filter(project => this.getDate(project.deadline) <= this.getDate(created_to));
      }
      if (status) {
        result = result.filter(project => project.status === status);
      }
      if (result === projects) {
        result = [];
      }
    }
    result.forEach(proj => {
      const year = new Date(proj.created_at).getFullYear();
      maxYear = Math.max(maxYear, year);
    });
    this.setState({ maxYear, projects: result });
  }

  getDate = (value) => {
    return new Date(value);
  }

  getUserName = (id, type) => {
    let user = {};
    if (type === "client") {
      user = this.context.clients.find(cl => cl._id === id);
    } else {
      user = this.context.workers.find(wo => wo._id === id);
    }
    return user.fio;
  }

  render() {
    const {
      clients,
      workers,
      allProjects,
    } = this.context;
    const { projects, maxYear } = this.state;

    return (
      <div>
        <Header onClick={this.handleDropdownMenuClick} />
        <div className="app-content row">
          <Menu onClick={this.handleMenuPointClick} />
          <div className="tables-module" style={{ width: "60%" }}>
            <div className="row">
              <h1>Статистика</h1>
              <Filter onFilter={this.setFilteredData} />
            </div>
            <div className="row" style={{ justifyContent: "space-between" }}>
              <div style={{ textAlign: "left" }}>
                <p>Клиентов: {clients.length}</p>
                <p>Менеджеров: {workers.length}</p>
                <p>Проектов: {projects.length}</p>
                <p>
                  Проектов, созданных в этом месяце({`${this.currentMonth}.${this.currentYear}`}):{" "}
                  {this.getThisMonthProjects(allProjects, "created_at")}
                </p>
                <p>
                  Проектов, заканчивающихся в этом месяце({`${this.currentMonth}.${this.currentYear}`}):{" "}
                  {this.getThisMonthProjects(allProjects, "deadline")}
                </p>
              </div>
              <div className="not-first">
                <>Проекты, сортрованные по статусам</>
                <Pie
                  data={this.getPieData}
                  height={200}
                  options={{
                    legend: {
                      position: "right",
                    },
                  }}
                />
              </div>
            </div>
            <div className="row bars">
              <div className="half">
                <p>Проекты, созданные за {maxYear} год</p>
                <Bar
                  data={this.getBarDataStart}
                  height={100}
                  options={{
                    legend: {
                      display: false,
                    },
                    scales: {
                      yAxes: [{
                        ticks: {
                          stepSize: 1,
                        }
                      }]
                    },
                  }}
                />
              </div>
              <div className="half">
                <p>Проекты, заканчивающиеся в {maxYear} году</p>
                <Bar
                  data={this.getBarDataFinish}
                  height={100}
                  options={{
                    legend: {
                      display: false,
                    },
                    scales: {
                      yAxes: [{
                        ticks: {
                          stepSize: 1,
                        }
                      }]
                    },
                  }}
                />
              </div>
            </div>
            <div className="row" style={{ justifyContent: "center", margin: "10px" }}>
              <ExcelFile
                element={(
                  <Button variant="outlined" color="primary">
                    Excel
                  </Button>
                )}
                filename={`Statictics for ${this.currentMonth + 1}.${this.currentYear}`}
                fileExtension="xlsx"
              >
                <ExcelSheet
                  dataSet={this.getExcelData()}
                  name={`${this.context.user.fio.split(" ")[0]} - ${this.currentMonth + 1}`}
                />
              </ExcelFile>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
