import React from "react";
import {
  Button,
  Tooltip,
  IconButton,
  TextField,
  CircularProgress,
} from "@material-ui/core";
import { Edit, Close } from "@material-ui/icons";
import Menu from "../components/menu";
import Header from "../components/header";
import Footer from "../components/footer";
import Avatar from "../components/avatar";
import SettingsContext from "../context";
import Snack from "../components/snack";
import { emailRegex, phoneRegex } from "./Registration";
import "./styles.scss";

export function getUsualDate(date) {
  const day = new Date(date).getDate();
  const month = new Date(date).getMonth() + 1;
  const year = new Date(date).getFullYear();
  const hours = new Date(date).getHours();
  const minutes = new Date(date).getMinutes();
  return `${dateCheck(day)}.${dateCheck(month)}.${year} ${dateCheck(hours)}:${dateCheck(minutes)}`;
}

function dateCheck(dateNumber) {
  return dateNumber < 10 ? `0${dateNumber}` : dateNumber;
}

export default class Profile extends React.Component {
  static contextType = SettingsContext;
  state = {
    isChanging: false,
    message: "",
    success: true,
    loading: false,
    userData: {
      phone: this.context.user.phone,
      fio: this.context.user.fio,
      email: this.context.user.email,
      description: this.context.user.description,
    },
    errors: {
      phone: false,
      fio: false,
      email: false,
    },
  }

  handleMenuPointClick = (path) => {
    this.props.history.push(path);
  }

  handleDropdownMenuClick = (path) => {
    this.props.history.push(path);
    if (path === "/auth") {
      this.context.logout();
    }
  }

  setChange = () => {
    this.setState({
      isChanging: !this.state.isChanging,
      errors: {
        phone: false,
        fio: false,
        email: false,
      }
    });
  }

  setNewValue = (event, field) => {
    this.setState({
      userData: { ...this.state.userData, [field]: `${event.target.value}` },
      errors: { ...this.state.errors, [field]: false }
    });
  }

  saveChanges = async () => {
    this.setState({ loading: true });
    const { phone, fio, email } = this.state.userData;
    const isEmailError = !emailRegex.test(email) || !email;
    const isPhoneError = !phoneRegex.test(phone) || !phone;
    if (isPhoneError || !fio || isEmailError) {
      this.setState({
        errors: {
          phone: isPhoneError,
          fio: !fio,
          email: isEmailError,
        },
        loading: false,
      });
      return;
    }
    const { message, success } = await this.context.updateUserInfo(this.state.userData);
    this.setState({ message, success });
    if (success) {
      this.setChange();
    }
    this.setState({ loading: false });
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ message: "" });
  }

  render() {
    const {
      isChanging,
      message,
      success,
      errors,
      userData,
      loading,
    } = this.state;
    const { user } = this.context;

    return (
      <>
        <div>
          <Header onClick={this.handleDropdownMenuClick} />
          <div className="app-content row">
            <Menu onClick={this.handleMenuPointClick} />
            <div className="info-module">
              {!isChanging ?
                <>
                  <div className="info-module_fio">
                    <Avatar width="80px" />
                    <div className="profile-name">
                      <h1 className="without_margins">{user.fio}</h1>
                      <p className="without_margins">{user.email}</p>
                    </div>
                    <div>
                      <Tooltip title={isChanging ? "Отменить" : "Изменить"}>
                        <IconButton onClick={this.setChange}>
                          {isChanging ? <Close /> : <Edit />}
                        </IconButton>
                      </Tooltip>
                    </div>
                  </div>
                  <div className="info-module_useful">
                    <table cellPadding="10px" className="table_profile">
                      <tbody>
                        <tr>
                          <td className="small-col">Тел.:</td>
                          <td>{user.phone}</td>
                        </tr>
                        <tr>
                          <td className="small-col">О себе:</td>
                          <td>{user.description}</td>
                        </tr>
                        <tr>
                          <td className="small-col">Проектов:</td>
                          <td>{user.projects}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <p className="created">
                    {user.created_at && `Дата создания пользователя: ${getUsualDate(user.created_at)}`}
                  </p>
                </>
                : <div className="info-module_useful">
                  <div className="cancel">
                    <Tooltip title="Отменить">
                      <IconButton onClick={this.setChange}>
                        <Close />
                      </IconButton>
                    </Tooltip>
                  </div>
                  <table cellPadding="10px" className="table_profile">
                    <tbody>
                      <tr>
                        <td className="small-col">ФИО:</td>
                        <td>
                          <TextField
                            defaultValue={user.fio}
                            onChange={(e) => this.setNewValue(e, "fio")}
                            variant="outlined"
                            fullWidth
                            error={errors.fio}
                            helperText={errors.fio && "Обязательное поле"}
                          />
                        </td>
                      </tr>
                      <tr>
                        <td className="small-col">Email:</td>
                        <td>
                          <TextField
                            defaultValue={user.email}
                            onChange={(e) => this.setNewValue(e, "email")}
                            variant="outlined"
                            fullWidth
                            error={errors.email}
                            helperText={errors.email && (!userData.email ? "Обязательное поле" : "Email адрес не валиден")}
                          />
                        </td>
                      </tr>
                      <tr>
                        <td className="small-col">Тел.:</td>
                        <td>
                          <TextField
                            defaultValue={user.phone}
                            onChange={(e) => this.setNewValue(e, "phone")}
                            variant="outlined"
                            fullWidth
                            error={errors.phone}
                            helperText={errors.phone && (!userData.phone ? "Обязательное поле" : "Не валидный номер")}
                          />
                        </td>
                      </tr>
                      <tr>
                        <td className="small-col">О себе:</td>
                        <td>
                          <TextField
                            defaultValue={user.description}
                            onChange={(e) => this.setNewValue(e, "description")}
                            variant="outlined"
                            multiline
                            rowsMax={15}
                            fullWidth
                          />
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <div className="auth-form__btn-wrapper">
                    <Button
                      variant="outlined"
                      color="primary"
                      className="auth-button center-item"
                      onClick={this.saveChanges}
                      disabled={loading}
                    >
                      Сохранить
                    </Button>
                    {loading && <CircularProgress size={24} className="btn-progress" />}
                  </div>
                </div>
              }
            </div>
          </div>
        </div>
        <Footer />
        <Snack
          open={!!message.length}
          handleClose={this.handleClose}
          text={message}
          type={success ? "success" : "error"}
        />
      </>
    );
  }
}
