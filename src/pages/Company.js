import React from "react";
import { Button, AppBar, Toolbar } from "@material-ui/core";
import Footer from "../components/footer";
import { Link } from "react-router-dom";
import "./styles.scss";
import "../components/styles.scss";

export default class Company extends React.Component {
	render() {
		return (
			<div>
				<div className="app-content">
					<div className="header">
						<AppBar position="static">
							<Toolbar className="header-div">
								<div className="company-name">
									<img
										src="https://x-lines.ru/letters/i/cyrillicscript/2607/ffffff/60/1/jj4zg7byjfky.png"
										alt="logo-fail"
										className="company-name_logo"
									/>
								</div>
							</Toolbar>
						</AppBar>
					</div>
					<div className="company-content">
						<img
							src="https://x-lines.ru/letters/i/cyrillicscript/2607/ffffff/60/1/jj4zg7byjfky.png"
							alt="logo-fail"
							className="company-name_logo center-item"
						/>
						<p style={{ marginTop: "50px" }}>ПРИ ПОДДЕРЖКЕ СЕРВИСА</p>
						<img
							style={{ margin: "50px auto" }}
							src="https://x-lines.ru/letters/i/cyrillicscript/2126/ffffff/60/1/kb3g64ufcp48gumbp3osq3e.png"
							alt="ProjectsManage logo"
							className="pm-logo center-item"
						/>
						<Button
							variant="outlined"
							color="primary"
							className="auth-button"
						>
							<Link to="/auth" className="link-btn">ProjectsManage</Link>
						</Button>
					</div>
				</div>
				<Footer />
			</div>
		);
	}
}
