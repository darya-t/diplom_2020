import React from "react";
import Menu from "../components/menu";
import Header from "../components/header";
import Footer from "../components/footer";
import Table from "../components/table";
import Snack from "../components/snack";
import Filter from "../components/filter";
import { CircularProgress } from "@material-ui/core";
import SettingsContext from "../context";
import "./styles.scss";

export default class Projects extends React.Component {
  static contextType = SettingsContext;

  state = {
    projects: this.context.user.role !== 1 && this.context.user.role !== 2
      ? this.context.allProjects
      : this.context.userProjects,
    message: "",
    success: false,
    loading: this.context.user.role !== 1 && this.context.user.role !== 2
      ? !this.context.allProjects.length
      : !this.context.userProjects.length,
  }

  componentDidMount() {
    setTimeout(() => {
      const contextProjects = this.context.user.role !== 1 && this.context.user.role !== 2
        ? this.context.allProjects
        : this.context.userProjects;
      if (this.state.projects !== contextProjects) {
        this.setState({ projects: contextProjects, loading: false });
      }
      this.setState({ loading: false });
    }, 3000);
  }

  handleMenuPointClick = (path) => {
    this.props.history.push(path);
  }

  handleDropdownMenuClick = (path) => {
    this.props.history.push(path);
    if (path === "/auth") {
      this.context.logout();
    }
  }

  updateTable = ({ message, success }) => {
    this.setState({
      projects: this.context.user.role !== 1 && this.context.user.role !== 2
        ? this.context.allProjects
        : this.context.userProjects,
      message,
      success,
    });
  }

  setFilteredData = (filter) => {
    const {
      name,
      client,
      manager,
      created_from,
      created_to,
      status,
    } = filter;
    const isFilter = name || client || manager || created_from || created_to || status;
    const { user, allProjects, userProjects } = this.context;
    const projects = user.role !== 1 && user.role !== 2 ? allProjects : userProjects;
    let result = projects;
    if (isFilter) {
      if (name) {
        result = result.filter(project => project.name.toLowerCase().includes(name.toLowerCase()));
      }
      if (client) {
        result = result.filter(project => this.getUserName(project.client, "client").includes(client));
      }
      if (manager) {
        result = result.filter(project => project.manager && this.getUserName(project.manager, "manager").includes(manager));
      }
      if (created_from) {
        result = result.filter(project => this.getDate(project.created_at) >= this.getDate(created_from));
      }
      if (created_to) {
        result = result.filter(project => this.getDate(project.deadline) <= this.getDate(created_to));
      }
      if (status) {
        result = result.filter(project => project.status === status);
      }
      if (result === projects) {
        result = [];
      }
    }
    this.setState({ projects: result });
  }

  getUserName = (id, type) => {
    let user = {};
    if (type === "client") {
      user = this.context.clients.find(cl => cl._id === id);
    } else {
      user = this.context.workers.find(wo => wo._id === id);
    }
    return user.fio;
  }

  getDate = (value) => {
    return new Date(value);
  }
  
  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ message: "" });
  }

  render() {
    const {
      projects,
      message,
      success,
      loading,
    } = this.state;

    return (
      <div>
        <Header onClick={this.handleDropdownMenuClick} />
        <div className="app-content row">
          <Menu onClick={this.handleMenuPointClick} />
          {
            loading ? <CircularProgress className="center-item" />
              : <div className="tables-module">
                <div className="row">
                  <h1 style={{ width: "-webkit-fill-available", marginLeft: "64px" }}>Проекты</h1>
                  <Filter onFilter={this.setFilteredData} />
                </div>
                {
                  projects.length
                    ? <>
                        <p>Нажмите на строку для просмотра подробной информации</p>
                        <Table
                          headers={["No", "Название", "Статус"]}
                          data={projects}
                          type="projects"
                          updateTable={this.updateTable}
                        />
                      </>
                    : <p>У Вас ещё нет проектов</p>
                }
              </div>
          }
        </div>
        <Footer />
        <Snack
          open={!!message.length}
          handleClose={this.handleClose}
          text={message}
          type={success ? "success" : "error"}
        />
      </div >
    );
  }
}
