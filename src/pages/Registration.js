import React from "react";
import {
  TextField,
  Button,
  Card,
  RadioGroup,
  FormControlLabel,
  Radio,
  CircularProgress,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import PasswordField from "../components/passwordField";
import "./styles.scss";
import Snack from "../components/snack";
import SettingsContext from "../context";

export const emailRegex = /.+@.+\..+/i;
export const phoneRegex = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/i;
export const passwordRegex = /^(?=.+[a-z])(?=.+[A-Z])(?=.+\d)[A-Za-z\d]{8,30}$/i;

export default class Registration extends React.Component {
  static contextType = SettingsContext;
  state = {
    user: {
      email: "",
      password: "",
      passwordRepeat: "",
      role: 0,
      fio: "",
      description: "",
      phone: "",
    },
    loading: false,
    isPwdShow1: false,
    isPwdShow2: false,
    message: "",
    errors: {
      email: false,
      password: false,
      passwordRepeat: false,
      fio: false,
      phone: false,
    }
  }

  setField = (e, field) => {
    this.setState({
      message: "",
      user: { ...this.state.user, [field]: e.target.value },
      errors: { ...this.state.errors, [field]: false }
    });
  }

  regiserValidation = async () => {
    this.setState({ loading: true });
    const { user } = this.state;
    const { email, password, fio, phone, passwordRepeat } = user;
    const isEmailError = !emailRegex.test(email) || !email;
    const isPwdError = !passwordRegex.test(password) || !password;
    const isRepPwdError = passwordRepeat !== password || !passwordRepeat;
    const isFioError = !fio;
    const isPhoneError = !phoneRegex.test(phone) || !phone;
    if (isEmailError || isPwdError || isRepPwdError || isFioError || isPhoneError) {
      this.setState({
        errors: {
          email: isEmailError,
          password: isPwdError,
          passwordRepeat: isRepPwdError,
          fio: isFioError,
          phone: isPhoneError,
        },
        loading: false,
      });
      return;
    }
    const newUser = {
      email: user.email,
      password: user.password,
      role: user.role,
      fio: user.fio,
      description: user.description,
      phone: user.phone,
    };
    const message = await this.context.register(newUser);
    if (message) {
      this.setState({ message });
    } else {
      this.props.history.push("/projects");
    }
    this.setState({ loading: false });
  }

  handleRadioChange = (event) => {
    this.setState({ user: { ...this.state.user, role: +event.target.value }, message: "" });
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ message: "" });
  }

  handleClickShowPassword = (field) => {
    this.setState({ [field]: !this.state[field] });
  }

  render() {
    const {
      user: {
        email,
        password,
        passwordRepeat,
        role,
        phone,
      },
      loading,
      isPwdShow1,
      isPwdShow2,
      message,
      errors
    } = this.state;

    return (
      <div>
        <img
          src="https://x-lines.ru/letters/i/cyrillicscript/2126/ffffff/60/1/kb3g64ufcp48gumbp3osq3e.png"
          alt="ProjectsManage logo"
          style={{
            margin: "10vh auto 10px auto",
          }}
        />
        <div className="authorization-div">
          <Card>
            <h2>Регистрация</h2>
            <form className="auth-form">
              <TextField
                label="ФИО"
                type="text"
                variant="outlined"
                className="auth-input"
                error={errors.fio}
                onKeyPress={this.handleEnterPress}
                helperText={errors.fio && "Обязательное поле"}
                onChange={(e) => this.setField(e, "fio")}
                required
              />
              <TextField
                label="Немного о себе"
                type="text"
                variant="outlined"
                className="auth-input"
                multiline
                rowsMax="4"
                onKeyPress={this.handleEnterPress}
                onChange={(e) => this.setField(e, "description")}
              />
              <div className="row cols">
                <TextField
                  label="Email"
                  variant="outlined"
                  type="email"
                  className="auth-input first-input"
                  error={errors.email}
                  helperText={errors.email && (!email ? "Обязательное поле" : "Email адрес не валиден")}
                  onChange={(e) => this.setField(e, "email")}
                  required
                />
                <TextField
                  label="Контактный номер"
                  type="text"
                  variant="outlined"
                  className="auth-input not-first-input"
                  error={errors.phone}
                  helperText={errors.phone && (!phone ? "Обязательное поле" : "Не валидный номер")}
                  onChange={(e) => this.setField(e, "phone")}
                  required
                />
              </div>
              <p>Выберите тип пользователя *</p>
              <RadioGroup onChange={this.handleRadioChange}>
                <FormControlLabel value="1" control={<Radio />} label="Клиент" />
                <FormControlLabel value="2" control={<Radio />} label="Менеджер" />
                <FormControlLabel value="3" control={<Radio />} label="Статистик" />
              </RadioGroup>
              <div className="row cols">
                <PasswordField
                  label="Пароль"
                  error={errors.password}
                  helperText={errors.password && (!password ? "Обязательное поле" : "Пароль должен быть не менее 8 символов, из которых как минимум 1 заглавная, 1 строчная буквы и 1 цифра")}
                  isPwdShow={isPwdShow1}
                  valueField="password"
                  isPwdShowField="isPwdShow1"
                  className="auth-input first-input"
                  labelWidth={60}
                  onChange={this.setField}
                  handleClickShowPassword={this.handleClickShowPassword}
                />
                <PasswordField
                  label="Повторите пароль"
                  error={errors.passwordRepeat}
                  helperText={errors.passwordRepeat && (!passwordRepeat ? "Обязательное поле" : "Пароли не совпадают")}
                  isPwdShow={isPwdShow2}
                  valueField="passwordRepeat"
                  isPwdShowField="isPwdShow2"
                  className="auth-input not-first-input"
                  labelWidth={140}
                  onChange={this.setField}
                  handleClickShowPassword={this.handleClickShowPassword}
                />
              </div>
              <Link className="link" to="/auth" >Авторизоваться</Link>
              <div className="auth-form__btn-wrapper">
                <Button
                  variant="outlined"
                  color="primary"
                  className="auth-button"
                  disabled={!role || loading}
                  onClick={this.regiserValidation}
                >
                  Зарегистрироваться
                </Button>
                {loading && <CircularProgress size={24} className="btn-progress" />}
              </div>
            </form>
          </Card>
        </div>
        <Snack
          open={!!message}
          handleClose={this.handleClose}
          text={message}
          type="error"
        />
      </div>
    );
  }
}
