import React from "react";
import Menu from "../components/menu";
import Header from "../components/header";
import Footer from "../components/footer";
import Table from "../components/table";
import Snack from "../components/snack";
import SettingsContext from "../context";
import { CircularProgress } from "@material-ui/core";
import "./styles.scss";

export default class NewProjects extends React.Component {
  static contextType = SettingsContext;

  state = {
    projects: this.context.newProjects,
    message: "",
    success: false,
    loading: !this.context.newProjects.length,
  }

  async UNSAFE_componentWillMount() {
    const { user } = this.context;
    if (user.role === 1) {
      this.props.history.push("/notfound");
    }
  }

  componentDidMount() {
    setTimeout(() => {
      if (this.state.projects !== this.context.newProjects) {
        this.setState({ projects: this.context.newProjects, loading: false });
      }
      this.setState({ loading: false });
    }, 3000);
  }

  handleMenuPointClick = (path) => {
    this.props.history.push(path);
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ message: "" });
  }

  handleDropdownMenuClick = (path) => {
    this.props.history.push(path);
    if (path === "/auth") {
      this.context.logout();
    }
  }

  updateTable = ({ message, success }) => {
    this.setState({
      projects: this.context.newProjects,
      message,
      success,
    });
  }

  render() {
    const {
      projects,
      message,
      success,
      loading,
    } = this.state;

    return (
      <div>
        <Header onClick={this.handleDropdownMenuClick} />
        <div className="app-content row">
          <Menu onClick={this.handleMenuPointClick} />
          {
            loading ? <CircularProgress className="center-item" />
              : <div className="tables-module">
                <h1>Новые проекты</h1>
                {
                  projects.length
                    ? <>
                        <p>Нажмите на строку для просмотра подробной информации</p>
                        {this.context.user.role !==2 && <p>Только менеджеры могуть выполнять проекты</p>}
                        <Table
                          headers={["No", "Название", "Статус", "Действие"]}
                          data={projects}
                          type="new"
                          updateTable={this.updateTable}
                        />
                      </>
                    : <p>Новых проектов ещё нет</p>
                }
              </div>
          }
        </div>
        <Footer />
        <Snack
          open={!!message.length}
          handleClose={this.handleClose}
          text={message}
          type={success ? "success" : "error"}
        />
      </div>
    );
  }
}
