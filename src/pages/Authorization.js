import React from "react";
import { Link } from "react-router-dom";
import {
  TextField,
  Button,
  Card,
  CircularProgress,
} from "@material-ui/core";
import PasswordField from "../components/passwordField";
import { emailRegex } from "./Registration";
import Snack from "../components/snack";
import SettingsContext from "../context";
import "./styles.scss";

export default class Authorization extends React.Component {
  static contextType = SettingsContext;
  state = {
    email: "",
    password: "",
    message: "",
    isPwdShow: false,
    loading: false,
    errors: {
      email: false,
      password: false
    }
  }

  setField = (e, field) => {
    this.setState({
      [field]: e.target.value,
      message: "",
      errors: { ...this.state.errors, [field]: false }
    });
  }

  authValidation = async () => {
    this.setState({ loading: true });
    const { email, password } = this.state;
    const isEmailError = !emailRegex.test(email) || !email;
    const isPwdError = !password;
    if (isEmailError || isPwdError) {
      this.setState({
        errors: {
          email: isEmailError,
          password: isPwdError,
        },
        loading: false,
      });
      return;
    }
    const message = await this.context.login(email, password);
    if (message) {
      this.setState({ message });
    } else {
      this.props.history.push("/projects");
    }
    this.setState({ loading: false });
  }

  handleEnterPress = (e) => {
    if (e.key === "Enter") {
      this.authValidation();
    }
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ message: "" });
  }

  handleClickShowPassword = (field) => {
    this.setState({ [field]: !this.state[field] });
  }

  render() {
    const {
      errors,
      message,
      isPwdShow,
      loading,
    } = this.state;

    return (
      <div>
        <img
          src="https://x-lines.ru/letters/i/cyrillicscript/2126/ffffff/60/1/kb3g64ufcp48gumbp3osq3e.png"
          alt="ProjectsManage logo"
          style={{
            margin: "16vh auto 20px auto",
          }}
        />
        <div className="authorization-div">
          <Card>
            <h2>Авторизация</h2>
            <form className="auth-form">
              <TextField
                label="Введите ваш email"
                variant="outlined"
                type="email"
                className="auth-input"
                error={errors.email}
                onKeyPress={this.handleEnterPress}
                helperText={errors.email && "Обязательное поле"}
                onChange={(e) => this.setField(e, "email")}
                required
              />
              <PasswordField
                label="Введите пароль"
                error={errors.password}
                helperText={errors.password && "Обязательное поле"}
                isPwdShow={isPwdShow}
                valueField="password"
                isPwdShowField="isPwdShow"
                className="auth-input with-top"
                labelWidth={130}
                onKeyPress={this.handleEnterPress}
                onChange={this.setField}
                handleClickShowPassword={this.handleClickShowPassword}
              />
              <Link className="link" to="/register" >Зарегистрироваться</Link>
              <div className="auth-form__btn-wrapper">
                <Button
                  variant="outlined"
                  color="primary"
                  className="auth-button"
                  onClick={this.authValidation}
                  disabled={loading}
                >
                  Войти
                </Button>
                {loading && <CircularProgress size={24} className="btn-progress" />}
              </div>
            </form>
          </Card>
        </div>
        <Snack
          open={!!message.length}
          handleClose={this.handleClose}
          text={message}
          type="error"
        />
      </div>
    );
  }
}
