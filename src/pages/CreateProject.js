import React from "react";
import {
  Button,
  TextField,
  CircularProgress,
} from "@material-ui/core";
import Menu from "../components/menu";
import Header from "../components/header";
import Footer from "../components/footer";
import Snack from "../components/snack";
import SettingsContext from "../context";
import "./styles.scss";

export function checkDeadlineDateValid(created, deadline) {
  const createdDate = new Date(created);
  const deadlineDate = new Date(deadline);
  if (deadlineDate >= createdDate) {
    return true;
  }
  return false;
}

export default class CreateProjects extends React.Component {
  static contextType = SettingsContext;
  state = {
    project: {
      name: "",
      deadline: "",
      description: "",
    },
    errors: {
      name: false,
      deadline: false,
    },
    loading: false,
    now: "",
    message: "",
    success: false,
  }

  async UNSAFE_componentWillMount() {
    if (this.context.user.role !== 1 && this.context.user.role !== 3) {
      this.props.history.push("/notfound");
    }
    await this.getCurrentDate();
  }

  handleMenuPointClick = (path) => {
    this.props.history.push(path);
  }

  handleDropdownMenuClick = (path) => {
    this.props.history.push(path);
    if (path === "/auth") {
      this.context.logout();
    }
  }

  setNewValue = (event, field) => {
    this.setState({
      project: { ...this.state.project, [field]: `${event.target.value}` },
      errors: { ...this.state.errors, [field]: false }
    });
  }

  getCurrentDate = () => {
    let now = new Date();
    const day = now.getDate() > 9 ? now.getDate() : `0${now.getDate()}`;
    const month = now.getMonth() > 9 ? now.getMonth() : `0${now.getMonth() + 1}`;
    now = `${now.getFullYear()}-${month}-${day}`;
    this.setState({
      now,
      project: { ...this.state.project, deadline: now },
    });
  }

  createProject = async () => {
    this.setState({ loading: true });
    const { project, now } = this.state;
    const isNameError = !project.name;
    const isDateError = project.deadline && !checkDeadlineDateValid(now, project.deadline);

    if (isNameError || isDateError) {
      this.setState({
        errors: {
          name: isNameError,
          deadline: isDateError,
        },
        loading: false,
      });
      return;
    }
    const newProject = {
      name: project.name,
      deadline: new Date(project.deadline),
      description: project.description,
      created_at: new Date(),
      client: localStorage.getItem("user"),
      status: "Новый",
    };
    const { message, success } = await this.context.projectCreate(newProject);
    this.setState({ message, success });
    if (success) {
      this.props.history.push("/projects");
    }
    this.setState({ loading: false });
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ message: "" });
  }

  render() {
    const {
      errors,
      project,
      message,
      success,
      loading,
    } = this.state;

    return (
      <div>
        <Header onClick={this.handleDropdownMenuClick} />
        <div className="app-content row">
          <Menu onClick={this.handleMenuPointClick} />
          <div className="info-module">
            <h1>Создание нового проекта</h1>
            <table cellPadding="10px" className="table_profile">
              <tbody>
                <tr>
                  <td className="small-col">Название:</td>
                  <td>
                    <TextField
                      onChange={(e) => this.setNewValue(e, "name")}
                      fullWidth
                      error={errors.name}
                      variant="outlined"
                      label="Название проекта"
                      helperText={errors.name && "Обязательное поле"}
                      required
                    />
                  </td>
                </tr>
                <tr>
                  <td className="small-col">Описание:</td>
                  <td>
                    <TextField
                      onChange={(e) => this.setNewValue(e, "description")}
                      variant="outlined"
                      multiline
                      rows={5}
                      rowsMax={15}
                      fullWidth
                    />
                  </td>
                </tr>
                <tr>
                  <td className="small-col">Время окончания проекта:</td>
                  <td>
                    <TextField
                      onChange={(e) => this.setNewValue(e, "deadline")}
                      fullWidth
                      variant="outlined"
                      type="date"
                      label="MM/DD/YYYY"
                      error={errors.deadline}
                      helperText={errors.deadline && "Дата не может быть раньше сегодняшнего дня"}
                      defaultValue={project.deadline}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </td>
                </tr>
              </tbody>
            </table>
            <div className="auth-form__btn-wrapper">
              <Button
                variant="outlined"
                color="primary"
                onClick={this.createProject}
                disabled={loading}
              >
                Создать
              </Button>
              {loading && <CircularProgress size={24} className="btn-progress" />}
            </div>
          </div>
        </div>
        <Footer />
        <Snack
          open={!!message.length}
          handleClose={this.handleClose}
          text={message}
          type={success ? "success" : "error"}
        />
      </div>
    );
  }
}
