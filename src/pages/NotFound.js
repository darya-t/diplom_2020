import React from "react";
import Footer from "../components/footer";

export default class NotFound extends React.Component {
  render() {
    return (
      <div className="not-found">
        <img
          src="https://x-lines.ru/letters/i/cyrillicscript/2126/ffffff/60/1/kb3g64ufcp48gumbp3osq3e.png"
          alt="ProjectsManage logo"
          style={{
            margin: "16vh auto 20px auto",
          }}
        />
        <h1>Страница не найдена</h1>
        <Footer />
      </div>
    );
  }
}
