import React from "react";

const SettingsContext = React.createContext();
const { Provider, Consumer } = SettingsContext;

const axios = require("axios");
const path = "https://projects-manage-server.herokuapp.com";
const md5 = require("js-md5");

class SettingsProvider extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: {},
			workers: [],
			clients: [],
			userProjects: [],
			newProjects: [],
			allProjects: [],
			login: this.login,
			logout: this.logout,
			register: this.register,
			projectCreate: this.projectCreate,
			getCurrentUser: this.getCurrentUser,
			getProjectByPM: this.getProjectByPM,
			updateUserInfo: this.updateUserInfo,
			updateProjectInfo: this.updateProjectInfo,
		};
	}

	getCurrentUser = async () => {
		if (localStorage.getItem("user")) {
			const user = localStorage.getItem("user");
			await axios.get(`${path}/users/current/${user}`)
				.then(async res => {
					const { data } = res;
					const role = data.role;
					let query = role === 1
						? { type: `byclient/${data._id}`, field: "userProjects" }
						: { type: `bymanager/${data._id}`, field: "userProjects" };
					const newUser = {
						...data,
						letters: this.getLetters(data.fio),
						projects: await this.getCMProjects(query),
					};
					this.setState({ user: newUser });
					if (role !== 1) {
						this.getCMProjects({ type: "new", field: "newProjects" });
					}
					await this.getCMProjects({ type: "all", field: "allProjects" });
					this.getClients();
					this.getWorkers();
				})
				.catch(err => {
					return err;
				})
		} else {
			return {};
		}
	}

	getLetters = (name) => {
		let fio = name.split(" ");
		let letters = "";
		fio.forEach((el, idx) => {
			if (idx >= 3) return;
			letters += el.slice(0, 1);
		});
		return letters;
	}

	getCMProjects = async ({ type, field }) => {
		let result = 0;
		await axios.get(`${path}/projects/${type}`)
			.then(async res => {
				const { data } = res;
				result = data.length;
				await this.setState({ [field]: data });
			})
			.catch(err => {
				this.notify(err);
			})
		return result;
	}

	login = async (email, password) => {
		let message = "";
		await axios.get(`${path}/users/login`, { params: { email, password: md5(password) } })
			.then(async res => {
				const { data } = res;
				if (!data) message = "Неверно введён логин или пароль";
				else {
					const role = data.role;
					let query = role === 1
						? { type: `byclient/${data._id}`, field: "userProjects" }
						: { type: `bymanager/${data._id}`, field: "userProjects" }
					const newUser = {
						...data,
						letters: this.getLetters(data.fio),
						projects: await this.getCMProjects({ ...query }),
					};
					this.setState({ user: newUser });
					if (role !== 1) {
						this.getCMProjects({ type: "new", field: "newProjects" });
					}
					await this.getCMProjects({ type: "all", field: "allProjects" });
					this.getClients();
					this.getWorkers();
					localStorage.setItem("user", data._id);
					message = "";
				}
			})
			.catch(err => {
				message = err.message;
			})
		return message;
	}

	register = async (user) => {
		let message = "";
		let newUser = {
			...user,
			password: md5(user.password),
			description: user.description || "-",
			created_at: new Date(),
		};
		await axios.post(`${path}/users/create`, newUser)
			.then(async res => {
				const { data } = res;
				if (data.message === "isUser") message = "Пользователь с таким логином уже существует";
				else if (data.message) message = data.message;
				else {
					const role = data.role;
					let query = role === 1
						? { type: `byclient/${data._id}`, field: "userProjects" }
						: { type: `bymanager/${data._id}`, field: "userProjects" }
					const newUser = {
						...data,
						letters: this.getLetters(data.fio),
						projects: await this.getCMProjects({ ...query }),
					};
					this.setState({ user: newUser });
					if (role !== 1) {
						this.getCMProjects({ type: "new", field: "newProjects" });
					}
					await this.getCMProjects({ type: "all", field: "allProjects" });
					this.getClients();
					this.getWorkers();
					localStorage.setItem("user", newUser._id);
					message = "";
				}
			})
			.catch(err => {
				message = err.message;
			})
		return message;
	}

	projectCreate = async (project) => {
		let result = {
			message: "",
			success: true
		};
		const newProject = {
			...project,
			description: project.description || "-",
			updated_at: new Date(),
		}
		const { user } = this.state;
		await axios.post(`${path}/projects/create`, newProject)
			.then(async res => {
				const { data } = res;
				if (data.message) {
					result = {
						message: data.message,
						success: false,
					};
				}
				else {
					this.getCMProjects({ type: `byclient/${localStorage.getItem("user")}`, field: "userProjects" });
					this.getCMProjects({ type: "all", field: "allProjects" });
					this.getCMProjects({ type: "new", field: "newProjects" });
					this.setState({ user: { ...user, projects: ++user.projects }});
					result = {
						message: "Проект успешно создан",
						success: true,
					};
				}
			})
			.catch(err => {
				result = {
					message: err.message,
					success: false,
				};
			})
		return result;
	}

	updateProjectInfo = async (changes) => {
		let newProject = {
			...changes,
			description: changes.description || "-",
			updated_at: new Date(),
		};
		if (changes.deadline) {
			newProject.deadline = new Date(changes.deadline);
		}
		let result = {
			message: "",
			success: true
		};
		await axios.put(`${path}/projects/update/${newProject._id}`, newProject)
			.then(async res => {
				const { data } = res;
				if (data.message) {
					result = {
						message: data.message,
						success: false
					};
				} else {
					let query = {
						type: `${this.state.user.role === 1 ? "byclient" : "bymanager"}/${localStorage.getItem("user")}`,
						field: "userProjects"
					};
					await this.getCMProjects(query);
					await this.getCMProjects({ type: "all", field: "allProjects" });
					await this.getCMProjects({ type: "new", field: "newProjects" });
					result = {
						message: "Изменения успешно сохранены",
						success: true
					};
				}
			})
			.catch(err => {
				result = {
					message: err.message,
					success: false
				};
			})
		return result;
	}

	getProjectByPM = async (projectId) => {
		let result = {
			message: "",
			success: true
		};
		await axios.get(`${path}/projects/current/${projectId}`)
			.then(async res => {
				const { data } = res;
				if (data.message) {
					result.message = data.message;
				}
				else {
					if (data.status === "Новый") {
						const newProject = {
							...data,
							status: "На рассмотрении",
							manager: localStorage.getItem("user"),
						}
						result = await this.updateProjectInfo(newProject);
					} else {
						result = {
							message: "Проект уже занят, обновите список",
							success: false,
						};
					}
				}
			})
			.catch(err => {
				result = {
					message: err.message,
					success: false,
				};
			})
		return result;
	}

	logout = () => {
		localStorage.setItem("user", "");
		this.setState({
			user: {},
			workers: [],
			userProjects: [],
			newProjects: [],
			allProjects: [],
		});
	}

	updateUserInfo = async (changes) => {
		let newUser = {
			...this.state.user,
			...changes,
			description: changes.description || "-",
		};
		let result = {
			message: "",
			success: true
		};
		await axios.put(`${path}/users/update/${newUser._id}/`, newUser)
			.then(async res => {
				const { data } = res;
				if (!data) {
					result = {
						message: "Что-то пошло не так",
						success: false
					};
				} else if (data.message && data.message === "isUser") {
					result = {
						message: "Выбранный логин занят",
						success: false
					};
				} else {
					this.setState({ user: newUser });
					result = {
						message: "Изменения успешно сохранены",
						success: true
					};
				}
			})
			.catch(err => {
				result = {
					message: err.message,
					success: false
				};
			})
		return result;
	}

	getWorkers = async () => {
		let workers = [];
		await axios.get(`${path}/users/workers`)
			.then(async res => {
				const { data } = res;
				workers = this.addUserProjects(data, "manager");
			})
		workers = this.sortUsers(workers);
		this.setState({ workers });
	}

	getClients = async () => {
		let clients = [];
		await axios.get(`${path}/users/clients`)
			.then(async res => {
				const { data } = res;
				clients = this.addUserProjects(data, "client");
			})
		clients = this.sortUsers(clients);
		this.setState({ clients });
	}

	sortUsers = (array) => {
		array.sort((a, b) => {
			const nameA = a.fio.toLowerCase();
			const nameB = b.fio.toLowerCase();
			if (nameA < nameB)
				return -1;
			if (nameA > nameB)
				return 1;
			return 0;
		});
		return array;
	}

	sortProjects = (array) => {
		array.sort((a, b) => {
			const dateA = new Date(a.created_at);
			const dateB = new Date(b.created_at);
			if (dateA < dateB)
				return -1;
			if (dateA > dateB)
				return 1;
			return 0;
		});
		return array;
	}

	addUserProjects = (array, type) => {
		array.forEach(user => {
			user.projects = this.state.allProjects.filter(project => project[type] === user._id).length;
			delete user.password;
		});
		return array;
	}

	render() {
		return <Provider value={this.state}>{this.props.children}</Provider>;
	}
}

function withContext(Component) {
	return function ConnectedComponent(props) {
		return (
			<SettingsContext.Consumer>
				{user => <Component {...props} userStore={user} />}
			</SettingsContext.Consumer>
		);
	};
}

export { SettingsProvider, Consumer as SettingsConsumer, withContext };
export default SettingsContext;
